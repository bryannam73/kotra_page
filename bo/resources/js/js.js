//달력
var defaultDatepicker = {
    dateFormat: 'yy-mm-dd',
    prevText: '이전 달',
    nextText: '다음 달',
    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
    dayNames: ['일', '월', '화', '수', '목', '금', '토'],
    dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
    dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
    showMonthAfterYear: true,
    yearSuffix: '년'
};
// 커스텀 달력
var customDatepicker = {
    dateFormat: 'yy-mm-dd',
    prevText: '이전 달',
    nextText: '다음 달',
    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
    dayNames: ['일', '월', '화', '수', '목', '금', '토'],
    dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
    dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
    showMonthAfterYear: true,
    yearSuffix: '년',
    dateFormat: 'yy-mm-dd', //Input Display Format 변경
   	onSelect: function (dateText, inst) {
   		location.href = '/ko/journeyCalendar?searchDate='+dateText;
	}
};

$(function () {
    if ($('.dPicker').length) {
        $('.dPicker').datepicker(defaultDatepicker);
    }
    // 모바일 MENU 내 저니캘린더
    if ($('#journey-calendar-datepicker').length) {
        $('#journey-calendar-datepicker').datepicker(customDatepicker);
    }

    $(".gnb__aside-datepicker").datepicker(customDatepicker);
    

    // 허브 메인을 비롯한 특정 서브 페이지에서는 header off 가 작동하지 않게 함.
    if (!$('.hubPg').length) {
        if ($('.gnb').hasClass('off')) {
            $('.gnb').removeClass('off')
        }
        if (!$('.header').hasClass('nav-down')) {
            $('.header').addClass('nav-down')
        }
    }

    //email 도메인 입력
    $(".input-email .selBox li").on('click', function(e){
        var blank = $(this).data('blank');

        if(blank == 'y'){
            $('.input-email__domain input').val('');
        } else {
            var selTxt = $(this).text();
            $('.input-email__domain input').val(selTxt);
        }
    });

    // tabMenu 2,3개 일 경우 layout 클래스 자동 부여
    if ($('.tabMenu').length) {
        $('.tabMenu').each(function () {
            let thisLength = $(this).children('li').length;
            if (thisLength === 2) {
                $(this).addClass('__layout2');
            }
            if (thisLength === 3) {
                $(this).addClass('__layout3');
            }
        });
    }

    //selbox 초기값 설정
    if ($('.selBox .selected').length) {
        $('.selBox .selected').each(function () {
            var selTxt = $(this).text();
            $(this).parents('.selBox').find('span').text(selTxt);
        });
    }
});

// s: 파일명 가져오기
var fileTarget = $('.file-box .file-hidden');
	
fileTarget.on('change', function(){
    if(window.FileReader){
        // 파일명 추출
        var filename = $(this)[0].files[0].name;
    } 
    else {
        // Old IE 파일명 추출
        var filename = $(this).val().split('/').pop().split('\\').pop();
    };

    $(this).siblings('.file-box-upload-name').val(filename);
});
// e: 파일명 가져오기



// pager indicator 가 있는 swiper 로딩
function loadSwiperList(el) {
    let swiperList = $(el + ' .swiper-slide'),
        swiperControlWrap = $(el + ' .swiper-pagination-wrap');
    if (swiperList.length > 1) {
        var swiperObj = new Swiper(el + ' .swiper-container', {
            loop: true,
            // simulateTouch: false,
            pagination: {
                el: el + ' .swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: el + ' .swiper-button-next',
                prevEl: el + ' .swiper-button-prev',
            },
            slidesPerView: 4,
            slidesPerGroup: 4,
            spaceBetween: 32,
            breakpoints: {
                1100: {
                    slidesPerView: 2,
                    slidesPerGroup: 2,
                    spaceBetween: 20,
                },
                767: {
                    slidesPerView: 1,
                    slidesPerGroup: 1,
                    spaceBetween: 20,
                },
            },
        });
    } else {
        swiperList.show();
        swiperControlWrap.hide();
    }
}

// header 영역 계산
function calcHeaderHeight() {
    var headH = $(".header").height();
    if($('.hubPg').length){
        $('.wrapper').css('paddingTop','0');
    } else {
        $('.wrapper').css('paddingTop',headH);
    }
}
$(window).on('resize', function () {
    let winWidth = $(window).width();
    if (winWidth > 767) {
        calcHeaderHeight();
        $('html').removeClass('is-clipped');
        $('.button-mobilemenu-open').removeClass('is-active');
        $('body')
            .removeClass('__mobile-menu-open')
            .removeClass('__mobile-calendar-open')
            .removeClass('__mobile-language-open');
    }
})
$(function () {
    /*header 영역 계산 */
    calcHeaderHeight();

    
    /* input field */
    // $(".iptWrap input").on('click', function(){
    //     $(this).addClass('focus');
    // });
    // $(".iptWrap button").on('click', function(){
    //     $(this).prev().val('').removeClass('focus');
    // });
    // $("input").on('blur', function(){
    //     if(!$(this).hasClass('focus')){
    //         $(this).removeClass('focus');
    //     }        
    // });    

    //select box
    $(".selBox span").on('click', function(e){
        if($(e.target).parent().hasClass('on')){
            $(".selBox").removeClass('on');
        } else if($(e.target).parent().hasClass('disabled')){
            $(".selBox").removeClass('on');
        } else {
            $(".selBox").removeClass('on');
            $(this).parent().addClass('on');
        }
    });
    $(".selBox li").on('click', function(e){
        var selTxt = $(this).text();
        $(this).siblings().removeClass();
        $(this).addClass('selected').parents('.selBox').find('span').text(selTxt);
        $(".selBox").removeClass('on');
    });

    //textarea counter
    $('.txtArea textarea').each(function () {
        let content = $(this).val();
        $(this).parent('.txtArea').find('.txtArea__count--check').text(content.length);
        
    });

    $('.txtArea textarea').on('keyup', function () {
        let content = $(this).val();
        let thisMaxText = $(this).parent('.txtArea').find('.txtArea__count--all').text();
        $(this).parent('.txtArea').find('.txtArea__count--check').text(content.length);

        if (content.length > thisMaxText) {
            alert('최대 ' + thisMaxText + '자까지 입력 가능합니다.');
            $(this).val(content.substring(0, thisMaxText));
            $(this).parent('.txtArea').find('.txtArea__count--check').text(content.length);
            return;
        }
        
    });


    // 외부영역 클릭 시 팝업 닫기
    $("body").on('click', function(e){
        var layPop = $('.lpopup'); 

        if($(e.target).hasClass('openPop')){
            layPop.addClass('on');
        } else if (!layPop.has(e.target).length){
            layPop.removeClass('on');
        }
    });

    //popup 열기
    $(".popOpen").on('click', function(){
        var popname = $(this).data('popname');
        $('.'+popname+'').addClass('on');

        $('.popDimm').addClass('on');
        $('body').addClass('overHidn');

        return false;
    });

    //popup 닫기
    $(".pop-close, .popClose").on('click', function(){
        $(this).closest('.pop').removeClass('on');
        $('.popDimm').removeClass('on');
        $('body').removeClass('overHidn');
        return false;
    });

    //pop 닫기
    $(".popHead--close, .pop-btns__close").on('click', function(){
        $(this).parents('.pop').removeClass('on');
        $('.popDimm').removeClass('on');
        $('body').removeClass('overHidn');
    });
    
    // 앱용 fixed closed 버튼 닫기
    $(".app-fixed-pop-close").on('click', function(){
        $(this).prev('.pop').removeClass('on');
        $('.popDimm').removeClass('on');
        $('body').removeClass('overHidn');
    });
    
    $(".main-pop__close").on('click', function(){
        $(this).parents('.main-pop').removeClass('on');
        if($(".main-pop.on").length == 0){            
            $('.popDimm').removeClass('on');
            $('body').removeClass('overHidn');
        }       
    });      
    // $(".popDimm").on('click', function(){
    //     $(this).removeClass('on');
    //     $('body').removeClass('overHidn');
    //     $('.pop').removeClass('on');
    // });  


    //공유하기 팝업 열기
    $(".btn-share").on('click', function(){
        $('.pop-share-sns, .popDimm').addClass('on');        
    });


    //상단 우측 서브MENU 열/닫기
    $('.gnb__aside div > button').on('mouseover',function(){
        $('.gnb__aside--list, .gnb__aside-datepicker').removeClass('on');
        $(this).next('.gnb__aside--list, .gnb__aside-datepicker').addClass('on');
    });        
    $('.gnb__aside--list, .gnb__aside-datepicker').on('mouseleave',function(){
        $(this).removeClass('on');
    });
    

    //gnb 마우스 오버
    $('.hubMain').on('mouseover',function(){
        $(this).removeClass('off');
    });
    $('.header').on('mouseleave',function(){
        $(this).addClass('off');
    });

    // $('.gnb__aside-cal').on('click', function (e) {
    //     e.preventDefault();
    //     $('.gnb__aside-datepicker').toggleClass('on');
    //     $('.gnb__aside--list').removeClass('on');
    // });

    $('.button-gnb-aside-language').on('click', function (e) {
        e.preventDefault();
        if ($('body').hasClass('__mobile-language-open')) {
            $('body').removeClass('__mobile-language-open');
        } else {
            $('body').addClass('__mobile-language-open').removeClass('__mobile-calendar-open');
        }
    });
    $('.button-gnb-aside-calendar').on('click', function (e) {
        e.preventDefault();
        if ($('body').hasClass('__mobile-calendar-open')) {
            $('body').removeClass('__mobile-calendar-open');
        } else {
            $('body').addClass('__mobile-calendar-open').removeClass('__mobile-language-open');
        }
    });

    //sub menu 열기
    $('.gnb__nav > ul > li > a').on('mouseover', function () {
        if (!$('body').hasClass('__mobile-menu-open')) {
            $('.gnb__nav-Sub').removeClass('on');
            $(this).next('.gnb__nav-Sub').addClass('on');
        }
    }).on('click', function (e) {
        if ($('body').hasClass('__mobile-menu-open')) {
            if ($(this).hasClass('has-child')) {
                e.preventDefault();    
            }
        }
        if ($(this).next('.gnb__nav-Sub').hasClass('on')) {
            $(this).removeClass('is-active');
            $('.gnb__nav-Sub').removeClass('on');
        } else {
            $(this).addClass('is-active');
            $('.gnb__nav-Sub').removeClass('on');
            $(this).next('.gnb__nav-Sub').addClass('on');
        }
    });
    $('.gnb__nav-Sub').on('mouseleave', function () {
        if (!$('body').hasClass('__mobile-menu-open')) {
            $(this).removeClass('on');
        }
    });

    // 모바일 gnb 를 위한 has-child 속성 부여
    // 202106
    /*
    $('.gnb__nav > ul > li').each(function () {
        if ($(this).find('div').length) {
            $(this).children('a').addClass('has-child');
            let thisHref = $(this).children('a').attr('href');
            $(this).find('.gnb__nav-Sub--menu').prepend('<li class="is-mobile-show"><a href="' + thisHref + '">Overview</a></li>');
        }
    });*/
    // 202107
    $('.allMenu-body > li').each(function () {
        if ($(this).find('.allMenu--sub').length) {
            let thisHref = $(this).children('a').attr('href');
            $(this).find('.allMenu--sub > ul').prepend('<li class="is-mobile-show"><a href="' + thisHref + '">Overview</a></li>');
        }
    });
    
    

    //header 동작
    var didScroll; 
    var lastScrollTop = 0; 
    var delta = 10; 
    
    $(window).scroll(function(event){ 
        didScroll = true;
        hasScrolled();
        $('.gnb__aside--list, .allMenu').removeClass('on');
        $(".selBox").removeClass('on');
    });
    
    // setInterval(function() { 
    //     if (didScroll) { 
    //         hasScrolled();
    //         didScroll = false; 
    //     } 
    // }, 250); 
    
    function hasScrolled() {
        let st = $(this).scrollTop();
        let navbarHeight = $('.header').outerHeight(); 
        
        if(Math.abs(lastScrollTop - st) <= delta) 
            return;
        
        if (st < delta) {
        } else {
            if (st < lastScrollTop) {
                // up scroll code
                $('.header').removeClass('nav-up').addClass('nav-down');
                $('html').removeClass('__nav-up').addClass('__nav-down');
                // type-b 동작제어
                typeBHeightCalc(navbarHeight);
            } else {
                // down scroll code
                if (st > navbarHeight) {
                    $('.header').removeClass('nav-down').addClass('nav-up');
                    $('html').removeClass('__nav-down').addClass('__nav-up');
                    // type-b 동작제어
                    if (st < 500) {
                        typeBHeightCalc(0);        
                    }
                }
            } 
        }
            
        // if (st > lastScrollTop && st > navbarHeight) {
        //     $('.header').removeClass('nav-down').addClass('nav-up');
        //     // $('.header').css({ 'top': '-' + navbarHeight + 'px' });
        //     // type-b 동작제어
        //     typeBHeightCalc(0);
        // } else if (st < delta) {
        //     $('.header').removeClass('nav-down').removeClass('nav-up');
        //     // $('.header').css({ 'top': '0px' });
        //     // type-b 동작제어
        //     typeBHeightCalc(navbarHeight);
        // } else { 
        //     if (st + $(window).height() < $(document).height()) {
        //         $('.header').removeClass('nav-up').addClass('nav-down');
        //         // $('.header').css({ 'top': '0px' });
        //         // type-b 동작제어
        //         typeBHeightCalc(navbarHeight);
        //     } 
        // } 
        lastScrollTop = st;
    }

    // Type B Visual 영역 처리
    function typeBVisualScroll() {
        if (!$('.type-b-visual').length) return;
        let st = $(this).scrollTop();
        if (st > 900) {
            $('.type-b-visual').css({ 'opacity': '0' });
        } else {
            $('.type-b-visual').css({ 'opacity': '1' });
        }
    }
    typeBVisualScroll();
    $(window).on('scroll', function () {
        typeBVisualScroll();
    });
    // B-Type 헤더, 비주얼 영역 높이 계산
    function typeBHeightCalc(headerH) {
        if (headerH === undefined) {
            var headerH = $('.header').height();   
        }
        if (!$('.type-b'.length)) return;
        let typeBVisualH = $('.type-b-visual').height();
        $('.type-b-visual').css({ 'top': headerH + 'px' });
        $('.type-b-content').css({ 'margin-top': headerH + typeBVisualH + 'px' });
    }
    // 헤더 탑 배너 동작
    $(document).on('click', '.header-bnr__close > button', function () {
        $(this).closest('.header-bnr').addClass('hidden');
        // 헤더 높이 계산
        calcHeaderHeight();
        // type-b 동작제어
        typeBHeightCalc();

    });
    
    //전체MENU 열기
    $('.gnb__aside-allMenu--btn').on('click',function(){
        $('.allMenu').addClass('on');
    });
    //전체MENU 닫기
    $('.allMenu-header--close').on('click',function(){
        $('.allMenu').removeClass('on');
    });        

    //전체MENU 펼침 아이콘
    $('.allMenu-body > li').each(function () {
        if($(this).find('.allMenu--sub').length){
            $(this).addClass('openIcn');
        }
    });

    // 툴바 모바일 전체 MENU
    $(document).on('click', '.button-mobilemenu-open', function (e) {
        e.preventDefault();
        if ($('body').hasClass('__mobile-menu-open')) {
            $('body').removeClass('__mobile-menu-open');
            $('html').removeClass('is-clipped');
            $(this).removeClass('is-active');
        } else {
            $('body').addClass('__mobile-menu-open');
            $('html').addClass('is-clipped');
            $(this).addClass('is-active');
        }
    }).on('click', '.button-mobilemenu-close', function (e) {
        e.preventDefault();
        $('body')
            .removeClass('__mobile-menu-open')
            .removeClass('__mobile-calendar-open')
            .removeClass('__mobile-language-open');
        $('html').removeClass('is-clipped');
        $('.button-mobilemenu-open').removeClass('is-active');
    });
        
    //전체MENU 동작
    $('.allMenu-body .openIcn > a').on('click',function(e){
        if($(e.target).next().hasClass('on')){
            $(this).parent().addClass('openIcn');
            $(this).next('.allMenu--sub').removeClass('on');
        } else{
            $(this).parent().removeClass('openIcn');
            $(this).next('.allMenu--sub').addClass('on');
        }
        return false;
    });

    // move to top 위치 이동 스크롤
    function check_scroll_gotop() {
        let st = $(window).scrollTop();

        if (st <= 200) {
            $('.footer--goTop').hide();
        } else {
            $('.footer--goTop').show();
        }
    }
    check_scroll_gotop();
    $(document).on('click', '.footer--goTop', function () {
        $('body, html').animate({ scrollTop: 0 });
    });
    $(window).on('scroll', function () {
		check_scroll_gotop();
    });
    
    // 푸터의 사업자정보 클릭시 펼침/닫힘
    $(document).on('click', '.button-footer-info', function () {
        if ($('.footer').hasClass('__mobile-footerinfo-active')) {
            $('.footer').removeClass('__mobile-footerinfo-active');
        } else {
            $('.footer').addClass('__mobile-footerinfo-active');
        }
    });

    //체크박스 전체 선택 및 해제
    $(".chk_all").on('click', function(){
        if($(".chk_all").is(":checked")){
            $(".chk").prop("checked", true);
        } else{
            $(".chk").prop("checked", false);
        }
    });
        
    // 한개의 체크박스 선택 해제시 전체선택 체크박스도 해제
    $(".chk").on('click', function(){
        var chkLen = $(".chk").length;
        if($(".chk:checked").length == chkLen){
            $(".chk_all").prop("checked", true);
        }else{
            $(".chk_all").prop("checked", false);
        }
    });		    
});

// s : input clear 처리
let $wrap = $(document).find('.wrapper');
function inputClear(){
    var $clearBtn = $wrap.find('.button-input-clear');
    $(document).on('click', '.button-input-clear', function(){
        var $this = $(this),
            $myInput = $this.closest('.input-text-wrap').find('.input-default');
        $myInput.val('');
        $this.removeClass('show');
    });
}
function inputDefault() {
    var $input = $wrap.find('.input-default'),
    $addButton = '<button type="button" class="button-input-clear">입력 내용 삭제</button>';
    if (!$input.length) return false;
    $input.each(function () {
        var $this = $(this);
        if (!$this.is('[title="address"]') && !$this.is('.datepicker') && !$this.is('[readonly]') && !$this.is('[disabled]')) {
            $this.wrap('<span class="input-text-wrap"></span>');
            $this.after($addButton);
        }
    })
    $input.on('input change', function(){
        var $this = $(this),
            $clearBtn = $this.closest('.input-text-wrap').find('.button-input-clear'),
            myVal = Boolean($this.val());
        $clearBtn.toggleClass('show', myVal);
        $this.css('padding-right','48px');
    }).trigger('propertychange');
    inputClear();
}
inputDefault();

$(document).on('focus', '.input-text-wrap > .input-default', function () {
    if ($(this).val().length > 0) {
        $(this).closest('.input-text-wrap').find('.button-input-clear').addClass('show');
    }
}).on('blur', '.input-text-wrap > .input-default', function () {
    let $this = $(this);
    setTimeout(function () {
        $this.css('padding-right','0').closest('.input-text-wrap').find('.button-input-clear').removeClass('show');
    }, 300);
        
});
// e : input clear 처리

/* 예약 페이지 좌측 MENU */
$('.rlm__link').on('mouseover',function(){
    $('.rlm__link').removeClass('on').next('.rlm__menu').removeClass('on');
    $(this).addClass('on').next('.rlm__menu').addClass('on');
});

$('.reservate__cnts').on('mouseover' ,function(){
	$('.rlm__link').removeClass('on').next('.rlm__menu').removeClass('on');
});

$('.rlm__menu--close-btn').on('click' ,function(){
    $('.rlm__link').removeClass('on').next('.rlm__menu').removeClass('on');
    return false;
});
/*//예약 페이지 좌측 MENU */

//예약페이지 선택 기능
$('.rcc__cal td').on('click' ,function(){
    if($(this).parents('.plural').length){
        // if($('.rcc__cal .on-last').length){
        //     $('.rcc__cal td').removeClass('on-first on-last term');
        //     $(this).addClass('on-first');
        // } else if($('.rcc__cal .on-first').length){
        //     $(this).addClass('on-last').prevUntil(".on-first").addClass('term');
        // } else {
        //     $(this).addClass('on-first');
        // }
    } else{
        $('.rcc__cal td').removeClass('on');
        $(this).addClass('on');
    }
});

// $('.plural td').on('click' ,function(){
//     console.log($('.plural .on').length);
    
//     $(this).css('border','1px solid red');
// });


