async function loadHTML(el, target) {
	let element = document.getElementById(el);
	element.innerHTML = await (await fetch(target)).text();
	// include 용 임시 : header 언어 선택 드롭다운용 스크립트
	var setHeaderDrop = function () {
		let $el = document.querySelector('#header').querySelector('.dropdown');
		$dropdowns.push($el);
		$el.addEventListener('click', function (event) {
			event.stopPropagation();
			let $dropdownHover = getAll('.dropdown:not(.is-hover)');
			$dropdownHover.forEach(function ($el) {
				$el.classList.remove('is-active');
			});
			$el.classList.toggle('is-active');
		});
		$el.addEventListener('mouseenter', function (event) {
			event.stopPropagation();
			$el.classList.toggle('is-hover');
		});
		$el.addEventListener('mouseleave', function (event) {
			event.stopPropagation();
			$el.classList.remove('is-hover');
		});
	};
	setHeaderDrop();

	// category button trigger
	new mlPushMenu(document.getElementById('mp-menu'), document.getElementById('trigger'), {
		type: 'cover',
	});
}
document.addEventListener('DOMContentLoaded', function () {
	loadHTML('header', './include/header.html');
});
document.addEventListener('DOMContentLoaded', function () {
	loadHTML('footer', './include/footer.html');
});

