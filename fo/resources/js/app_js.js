
$(function () {
    // s : 플로팅 키 클릭시 삼각함수 이용 하위 MENU 원형 배치
    function circleXY($el, degrees, r) {
        let sin = Math.sin((degrees * Math.PI) / 180);
        let cos = Math.cos((degrees * Math.PI) / 180);
        let x, y;
        x = cos * r;
        y = sin * r;
        $el.css({ top: y + 'px', left: x + 'px' });
    }

    $('.floating-button').on('click', function () {
        $('body').addClass('__floating-button');

        let deg = 90;
        let thisCount = $('.floating-button-list > li').length;
        $('.floating-button-list > li').each(function () {
            circleXY($(this), deg, 120);
            deg += 180 / (thisCount - 1);
        });
    });
    $('.floating-button-close').on('click', function () {
        $('body').removeClass('__floating-button');
        $('.floating-button-list > li').each(function () {
            $(this).css({ top: '0', left: '0' });
        });
    });
    // e : 플로팅 키 클릭시 삼각함수 이용 하위 MENU 원형 
    
    // 앱용 저니추가시 select box 비활성 -> 활성 처리
    $(".app-journey-box .selBox li").on('click', function (e) {
        var selTxt = $(this).text();
        $(this).siblings().removeClass();
        $(this).addClass('selected').parents('.selBox').find('span').text(selTxt);
        $(this).closest('.selBox').removeClass('is-disabled');
        $(".selBox").removeClass('on');
    });

    // 앱 저니플랜 상세 삭제/취소 스와이프
    var currentX;
    var lastX = 0;
    var lastT;
    $('.app-journey-detail-item > a').on('touchmove', function (e) {
        if ($(this).closest('.app-journey-detail--past').length) {
            return;
        }
        e.stopPropagation();
        // If still moving clear last setTimeout
        clearTimeout(lastT);
        let currentX = e.originalEvent.touches[0].clientX;
        let currentY = e.originalEvent.touches[0].clientY;
        
        // After stoping or first moving
        if(lastX == 0) {
            lastX = currentX;
            lastY = currentY;
        }
        // X축, Y축으로 터치 움직인 거리를 비교해 좌우 스와이프 여부 결졍
        let deltaX = Math.abs(lastX - currentX)
        let deltaY = Math.abs(lastY - currentY)

        if (deltaX > deltaY) {
            if (currentX < lastX) {
                // Swipe to Left
                $(this).addClass('is-swipeleft');
            } else if (currentX > lastX) {
                // Swipe to Right
                $(this).removeClass('is-swipeleft');
            } else {
                return;
            }
        }
        // Save last position
        lastX = currentX;
        lastY = currentY;
        // Check if moving is done
        lastT = setTimeout(function() {
            lastX = 0;
            lastY = 0;
        }, 100);
        
    }).on('click', function (e) {
        e.preventDefault();
    });

});

// 앱용 토스트 알림
function toastAlert(txt) {
    $('body').append('<div class="ui-toast-alert"><strong></strong></div>');
    var $toast = $('.ui-toast-alert');
    setTimeout(function(){
	    $toast.addClass('is-active').children('strong').text(txt);
	}, 100);
	setTimeout(function(){
        $toast.removeClass('is-active');
	}, 2000);
	setTimeout(function(){
        $toast.remove();
	}, 2500);
}

// 앱용 pager indicator 가 있는 swiper 로딩
function loadAppPagerSwiper(el) {
    let swiperList = $(el + ' .swiper-slide'),
        swiperControlWrap = $(el + ' .swiper-pagination-wrap');
    if (swiperList.length > 1) {
        var swiperObj = new Swiper(el + ' .swiper-container', {
            loop: true,
            // simulateTouch: false,
            pagination: {
                el: el + ' .swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: el + ' .swiper-button-next',
                prevEl: el + ' .swiper-button-prev',
            },
            slidesPerView: 1,
            slidesPerGroup: 1,
            spaceBetween: 20,
            autoHeight:true,
        });
    } else {
        swiperList.show();
        swiperControlWrap.hide();
    }
}

// 앱용 pager indicator 가 있는 swiper 로딩
// 2개를 기본으로 보이게 하는 슬라이드
function loadAppPagerMultiSwiper(el) {
    let swiperList = $(el + ' .swiper-slide'),
        swiperControlWrap = $(el + ' .swiper-pagination-wrap');
    if (swiperList.length > 1) {
        var swiperObj = new Swiper(el + ' .swiper-container', {
            loop: false,
            // simulateTouch: false,
            pagination: {
                el: el + ' .swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: el + ' .swiper-button-next',
                prevEl: el + ' .swiper-button-prev',
            },
            slidesPerView: 2.5,
            slidesPerGroup: 2,
            spaceBetween: 15,
            on: {
                slideChangeTransitionStart: function () {
                    $(el).addClass('hide-swiper-hint');
                },
                slideChangeTransitionEnd: function () {
                    $(el).removeClass('hide-swiper-hint');
                },
            },
        });
    } else {
        swiperList.show();
        swiperControlWrap.hide();
    }
}

$(document).on('click', '[role=tabmenu]', function (e) {
    e.preventDefault();
    thisHref = $(this).attr('href');
    $(this).parent('li').addClass('on').siblings('li').removeClass('on');
    $(thisHref).show().siblings('.tabpanel').hide();
});

// 앱 메인 : 스크롤에 따른 water 배경 효과
var animation = function () {
    var items, winH;
   
    var initModule = function () {
      items = document.querySelectorAll(".circle-wave");
      winH = window.innerHeight;
      _addEventHandlers();
    }
   
    var _addEventHandlers = function () {
      window.addEventListener("scroll", _checkPosition);
      window.addEventListener("load", _checkPosition);
      window.addEventListener("resize", initModule);
    };
   
    var _checkPosition = function () {
      for (var i = 0; i < items.length; i++) {
        var posFromTop = items[i].getBoundingClientRect().top;
        if (winH > posFromTop) {
          items[i].classList.add("active");
        }
      }
    }
   
    return {
      init: initModule
    }
  }
   
  animation().init();